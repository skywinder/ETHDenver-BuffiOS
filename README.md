# BuffiOS - Secure & Fast Ethereum/Dai/xDai Wallet for loans

<img src="https://cdn-images-1.medium.com/max/1600/1*A6RQ55zvAhSh6nEvVH_yNg.jpeg" align="center" width="300">

## Freedom is **essential**. Privacy **Matter**!

### What is it?

- Easy and secure way to send money through Ethereum Networks 
- Easy swap Ether, Dai and xDai
- Send xDai quickly and secure
- Send Buffi token
- Lending platform

### So, let's go deeper into those statements:

### Features
- Support xDai
- Support classic Ethereum Networks
- Let you be **free from hard-bonded solution** like (`Laptop + Chrome + MetaMask + Private key`) binding. 
- **No private key sharing** needed! Store it at one place, sign anywhere!

## Purpose

We built a native xDai-based lending platform with user-friendly wallet, cross-application coordination (via deep links) to provide p2p micro-loans to users willing to buy goods in various marketplaces with borrowed xDai or lend xDai to their peers earning interest on the deposit of funds on our platform. 

## Our model use case 
If I have spent all my buffiDai coins on food already but still hungry, it allows me to borrow 10 buffiDai coins from another ETH Denver participant, who still has them, and return it as 11 buffiDai coins exactly one year later on ETH Denver 2020.

 ## User-flow 1

1. Open the BuffiOS wallet app on the phone
2. Pay (your debts or make a purchase in a marketplace), deposit your fund to earn % or take credit from your peers.
3. Profit!

 ## User-flow 2
1. Open a marketplace of your choice (connected via [deep links](https://devpost.com/software/divelane) to the BuffiOS wallet app on your phone)
2. Choose anything you like and go for checkout
3. Scan QR code, choose to pay with Credit or Debit
4. If you prefer Debit - the screen with the amount borrowed, payback period and % applied shows up
5. Enjoy!

## Further development - next steps
1. We plan to add KYC to ensure the qulaity of the borrowers by linking our platform with uPort at the regidtration step
2. We will also build credit scoring with zero-knowledge proofs - in this setup a user will be given credits if his/her history of transactions on our platform is decent (smart contract checks if all previous debts were oaid back on time), but there is no need to reveal private details (exact sum previouslz borrowed, from whom, etc)
